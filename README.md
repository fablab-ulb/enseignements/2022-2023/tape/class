# Techniques Avancées de Physique Expérimentale TAPE 2022-2023 (partie 1)

Titulaire : Prof. Denis Terwagne  
Cours ouvert aux étudiants en Master en physique de l'Université Livre de Bruxelles (ULB)  

Dans ce cours, l'étudiant apprendra à développer un processus d'exploration technique pour gagner en maîtrise d'un aspect d'un outil scientifique experimental et à le transmettre.

Plus particulièrement, nous travaillerons cette année, dans le cadre d'un projet de recherche de coopération avec Cuba, sur un outil de microscopie low-cost, le Foldscope, un microscope en papier qui se plie comme un origami.

![image d'un foldscope](img/foldscope.jpg)

Le défi sera d'évaluer les possibilités d'utiliser un tel microscope pour l'imagerie et la détection de microparticules dans l'océan (microplastique, planctons, ...).

## Etudiants

* [Simon Biot](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/simon.biot)
* [Nasra Daher](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/nasra.daher)
* [Alexandre Hallemans](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/alexandre.hallemans)
* [Arthur Robert](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/arthur.robert)
* [Danae Valdenaire](https://gitlab.com/fablab-ulb/enseignements/2022-2023/tape/students/danae.valdenaire/-/blob/main/Valdenaire_Pr%C3%A9sentation.md)

## Exercices

### Ex. 0 - Le journal de bord

Tout au long de ce cours, vous rendrez compte de votre processus d'exploration sous la forme d'un journal de bord qui allie narration et argumentation scientifique.

Commencez le journal en vous présentant brièvement. Jour après jour rendez compte de vos explorations suivant les exercices donnés ci-dessous, décrivez les difficultés rencontrées, vos échecs et comment vous les avez surmontés.

Pour ce faire, vous utiliserez la plate-forme GitLab du cours et des outils de compression d'images tel que GIMP.   
Afin de limiter l'impact sur la planète, la taille maximale de votre espace ne devra pas excéder 25Mo.

### Ex. 1 - L'outil scientifique

Regardez cette vidéo TED du physicien Manu Prakash de l'Université de Stanford présentant le Foldscope : [A 50 cents microscope that folds like origami](https://www.ted.com/talks/manu_prakash_a_50_cent_microscope_that_folds_like_origami)

Lisez attentivement et en profondeur [la publication scientifique présentant cet outil](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781)

Identifiez trois aspects techniques remarquables de cet outil et décrivez dans votre journal de bord pourquoi ils sont remarquables.

### Ex. 2 - Montage et premières observations

Montez un microscope Foldscope et faites une observation de quelque chose qui se trouve dans votre environnement quotidien.  
Réalisez une mesure de longueur/aire à l'aide d'une grille de calibration.

Relatez votre expérience de montage (était-ce facile ? Pas facile ? Pourquoi ? ...) ainsi que votre observation et mesure expérimentales dans votre journal de bord.

### Ex. 3 - Etat de l'art

Faites l'état de l'art sur les possibilités techniques et d'applications offertes par le Foldoscope. Plongez-vous dans la littérature scientifique et présentez 3 applications ou faits techniques que vous trouvez intéressants dans votre journal de bord.

### Ex. 4 - Défi et exploration

Parmi les multiples aspects techniques du Foldscope relevés en classe, déterminez et faites l'état de l'art d'un aspect que vous souhaiteriez explorer plus en profondeur et maîtriser.

Relatez votre processus d'exploration dans votre journal de bord. Décrivez vos échecs et comment vous les avez surmontés. Qu'avez-vous appris ?

### Ex. 5 - Conclusion et transmission

Au terme de votre exploration, qu'avez-vous réussi à maîtriser ? Quel a été votre défi ?Qu'avez-vous appris ? Que voudriez-vous transmettre à la communauté qui utilise le Foldscope ou de la microscopie low-cost ?

Exposez vos conclusions de manières synthétiques, visuelles et pédagogiques pour transmettre efficacement votre maîtrise.

### Ex. 6 - Autoévaluation

Au terme de votre parcours, commentez les critères d'évaluation identifiés en classe et envoyez les par email au titulaire du cours. Plus d'instructions suivront.

Date limite pour finaliser le travail : lundi 28 Novembre 2022.

## Liens utiles

Frugal Science  

* [The Frugal Way, The Economist, 2011](https://gmwgroup.harvard.edu/publications/frugal-way)
* [Open-source lab](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/blob/9ca42c056c4371315ecdac35195d28098be02f0b/vade-mecum/open-source-lab.md)


Foldscope  

* [Ted talk : A 50-cent microscope that folds like origami, Manu Prakash](https://www.youtube.com/embed/h8cF5QPPmWU)
* Foldscope: Origami-Based Paper Microscope ([site web](https://foldscope.com/), [PLOS One paper](https://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0098781))
* [Communauté Microcosmos](https://microcosmos.foldscope.com/)

Other frugal science tools

* Paperfuge: An ultra-low cost, hand-powered centrifuge inspired by the mechanics of a whirligig toy ([news](https://www.wired.com/2017/01/paperfuge-20-cent-device-transform-health-care/), [video](https://www.youtube.com/watch?v=L5ppD07DMKQ),[paper on bioRxiv](https://www.biorxiv.org/content/10.1101/072207v1))

Outils numériques  

* [GitLab](https://gitlab.com/)
* [Markdown](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown)
* [GIMP](https://www.gimp.org/)
